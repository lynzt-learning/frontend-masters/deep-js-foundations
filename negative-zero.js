var foo = 0 / -3;
foo === -0;     // true
foo === 0;      // true
0 === -0;       // true
(0/-3) === (0/3)    // true

foo;            // 0


/***************************** */
function isNeg0(x) {
    return x === 0 && (1/x) === -Infinity;
}

isNeg0(0/-3);       // true
isNeg0(0/3);        // false

/******************* */
// use object.is wehn checking for nan and -0

Object.is("foo", NaN);  // false
Object.is(NaN, NaN);    // true

Object.is(0, -0)        // false
Object.is(-0, -0);      // true

