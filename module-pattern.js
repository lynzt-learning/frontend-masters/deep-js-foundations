var foo = (function () {
    var o = {bar: 'baz'};
    return {
        bar: () => console.log(o.bar)
    }
})();

foo.bar() // baz
foo.o;  // undefined

// **************************

var foo1 = function() {
    var o = {bar: 'baz'};

    return {
        bar: () => console.log(o.bar)
    }
};

const foo1 = foo1();
foo1.bar(); // baz
foo1.o; // undef

// **************************

var foo = function() {
    var publicApi = {
        bar: () => publicApi.baz(),
        baz: () => console.log('baz')
    };
    return publicApi;
}

var foo2 = foo();
foo2.bar();